


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:07:42 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
     end system

     do
        time dependent hf
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     method
        hf
     end method

     solver tdhf
       states: 2
       residual threshold: 1.0d-10
     end solver tdhf


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Time dependent Hartree-Fock engine
  ========================================

  Drives the calculation of the Hartree-Fock excitation energies and properties.

  This is a RHF properties calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)
     3) Calculate TDHF excitation energies


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492359869020
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796357979403     0.9791E-01     0.7880E+02
     2           -78.828453237454     0.7078E-01     0.3210E-01
     3           -78.843281517241     0.6747E-02     0.1483E-01
     4           -78.843608975051     0.2753E-02     0.3275E-03
     5           -78.843645123311     0.3974E-03     0.3615E-04
     6           -78.843646182586     0.5223E-04     0.1059E-05
     7           -78.843646204409     0.6084E-05     0.2182E-07
     8           -78.843646205153     0.2132E-05     0.7440E-09
     9           -78.843646205256     0.3153E-06     0.1024E-09
    10           -78.843646205256     0.2623E-07     0.3979E-12
    11           -78.843646205256     0.5559E-08     0.2842E-13
    12           -78.843646205256     0.1724E-08     0.9948E-13
    13           -78.843646205256     0.7589E-09     0.4263E-13
    14           -78.843646205256     0.1286E-09     0.0000E+00
    15           -78.843646205256     0.2324E-10     0.0000E+00
    16           -78.843646205256     0.4590E-11     0.0000E+00
    17           -78.843646205256     0.1330E-11     0.2842E-13
    18           -78.843646205256     0.1391E-12     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 18 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645693538853
     Nuclear repulsion energy:      12.167611407170
     Electronic energy:            -91.011257612426
     Total energy:                 -78.843646205256


  3) Calculate TDHF excitation energies

   - Davidson eigenvalue equation solver
  -----------------------------------------

  A Davidson solver that solves an eigenvalue equation: M x = omega x. 
  This equation is solved in a reduced space. A description of the algorithm 
  can be found in E. R. Davidson, J. Comput. Phys. 17, 87 (1975).

   - Davidson tool settings:

     Number of parameters:                  138
     Number of requested solutions:           2
     Max reduced space dimension:            50

     Storage (tdhf_davidson_trials): memory
     Storage (tdhf_davidson_transforms): memory

  - Davidson solver settings

     Number of singlet states:               2
     Max number of iterations:             100

  Iteration:                  1
  Reduced space dimension:    2

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.104822233347    0.000000000000     0.8089E+00   0.1048E+00
     2   0.173892785930    0.000000000000     0.7510E+00   0.1739E+00
  -------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.084705174734    0.000000000000     0.4467E+00   0.2012E-01
     2   0.153394425944    0.000000000000     0.4311E+00   0.2050E-01
  -------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078247723031    0.000000000000     0.1126E+00   0.6457E-02
     2   0.143343615360    0.000000000000     0.3843E+00   0.1005E-01
  -------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078175194117    0.000000000000     0.3088E-01   0.7253E-04
     2   0.142116849439    0.000000000000     0.1352E+00   0.1227E-02
  -------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078197860090    0.000000000000     0.1408E-02   0.2267E-04
     2   0.142320334311    0.000000000000     0.2398E-01   0.2035E-03
  -------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078190938421    0.000000000000     0.3026E-03   0.6922E-05
     2   0.142354475506    0.000000000000     0.3864E-02   0.3414E-04
  -------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189911285    0.000000000000     0.1175E-03   0.1027E-05
     2   0.142329910656    0.000000000000     0.1233E-02   0.2456E-04
  -------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189696960    0.000000000000     0.2831E-04   0.2143E-06
     2   0.142328377740    0.000000000000     0.2480E-03   0.1533E-05
  -------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189625655    0.000000000000     0.2098E-05   0.7130E-07
     2   0.142327785546    0.000000000000     0.3210E-04   0.5922E-06
  -------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   20

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189602845    0.000000000000     0.5306E-06   0.2281E-07
     2   0.142327775222    0.000000000000     0.5743E-05   0.1032E-07
  -------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   22

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601146    0.000000000000     0.1891E-06   0.1699E-08
     2   0.142327766807    0.000000000000     0.1753E-05   0.8415E-08
  -------------------------------------------------------------------------

  Iteration:                 12
  Reduced space dimension:   24

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601097    0.000000000000     0.1393E-07   0.4820E-10
     2   0.142327765146    0.000000000000     0.3061E-06   0.1661E-08
  -------------------------------------------------------------------------

  Iteration:                 13
  Reduced space dimension:   26

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601107    0.000000000000     0.1927E-08   0.9295E-11
     2   0.142327765321    0.000000000000     0.1722E-07   0.1742E-09
  -------------------------------------------------------------------------

  Iteration:                 14
  Reduced space dimension:   28

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601107    0.000000000000     0.1292E-09   0.4961E-12
     2   0.142327765326    0.000000000000     0.2173E-08   0.5947E-11
  -------------------------------------------------------------------------

  Iteration:                 15
  Reduced space dimension:   30

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601107    0.000000000000     0.1448E-10   0.2104E-13
     2   0.142327765334    0.000000000000     0.7895E-09   0.7967E-11
  -------------------------------------------------------------------------

  Iteration:                 16
  Reduced space dimension:   31

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601107    0.000000000000     0.1387E-10   0.3671E-13
     2   0.142327765335    0.000000000000     0.2432E-09   0.9087E-13
  -------------------------------------------------------------------------

  Iteration:                 17
  Reduced space dimension:   32

   Root  omega (Re)        omega (Im)         |residual|   Delta omega (Re)
  -------------------------------------------------------------------------
     1   0.078189601107    0.000000000000     0.1407E-10   0.1339E-13
     2   0.142327765334    0.000000000000     0.2411E-10   0.5726E-13
  -------------------------------------------------------------------------
  Convergence criterion met in 17 iterations!

  - TDHF excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.279624035281        7.608957564515
        2                  0.377263522401       10.265863339321
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Timings for the RHF properties calculation

     Total wall time (sec):              6.98100
     Total cpu time (sec):              10.92330

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 232.156 KB

  Total wall time in eT (sec):              7.03000
  Total cpu time in eT (sec):              10.97133

  Calculation ended: 2022-02-01 16:07:49 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
