


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:09:46 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
     end system

     memory
       available: 8
     end memory

     do
       excited state
     end do

     method
        hf
        cc2
     end method

     cc
       bath orbital
     end cc

     solver scf
        gradient threshold: 1.0d-11
        energy threshold: 1.0d-11
     end solver scf

     solver cholesky
        threshold: 1.0d-11
     end solver cholesky

     solver cc gs
        omega threshold: 1.0d-11
     end solver cc gs

     solver cc es
        ionization
        residual threshold: 1.0d-11
        singlet states: 2
        algorithm: davidson
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.100000000000    -0.020000000000     7.530000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.188972612457    -0.037794522491    14.229637717975        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               29
     Number of orthonormal atomic orbitals:   29

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         23
     Number of molecular orbitals:       29


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.492022836321
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.796606592585     0.9786E-01     0.7880E+02
     2           -78.828675852654     0.7077E-01     0.3207E-01
     3           -78.843487343819     0.6747E-02     0.1481E-01
     4           -78.843814479549     0.2753E-02     0.3271E-03
     5           -78.843850612079     0.3973E-03     0.3613E-04
     6           -78.843851670925     0.5220E-04     0.1059E-05
     7           -78.843851692779     0.6096E-05     0.2185E-07
     8           -78.843851693528     0.2137E-05     0.7488E-09
     9           -78.843851693630     0.3151E-06     0.1027E-09
    10           -78.843851693631     0.2637E-07     0.4547E-12
    11           -78.843851693631     0.5634E-08     0.4263E-13
    12           -78.843851693631     0.1415E-08     0.5684E-13
    13           -78.843851693631     0.6806E-09     0.2842E-13
    14           -78.843851693631     0.1491E-09     0.2842E-13
    15           -78.843851693631     0.2501E-10     0.8527E-13
    16           -78.843851693631     0.5166E-11     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.645629080245
     Nuclear repulsion energy:      12.116100574587
     Electronic energy:            -90.959952268218
     Total energy:                 -78.843851693631

  - Timings for the RHF ground state calculation

     Total wall time (sec):              0.95600
     Total cpu time (sec):               1.66838


  :: CC2 wavefunction
  ======================

     Bath orbital(s):         True

   - Number of orbitals:

     Occupied orbitals:    6
     Virtual orbitals:     24
     Molecular orbitals:   30
     Atomic orbitals:      29

   - Number of ground state amplitudes:

     Single excitation amplitudes:  144
     Double excitation amplitudes:  10440


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CC2 excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-10
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    29
     Total number of shell pairs:           120
     Total number of AO pairs:              435

     Significant shell pairs:               117
     Significant AO pairs:                  430

     Construct shell pairs:                 120
     Construct AO pairs:                    435

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               405 /     107       0.47383E+01         147             42             17010
     2               318 /      92       0.47165E-01         234            111             35298
     3               246 /      74       0.46944E-03         178            183             45018
     4               173 /      51       0.38270E-05         145            265             45845
     5                70 /      18       0.38106E-07          78            324             22680
     6                 0 /       0       0.37202E-09          33            345                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 345

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.6608E-11
     Minimal element of difference between approximate and actual diagonal:  -0.1238E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       False

  - Finished decomposing the ERIs.

     Total wall time (sec):              0.44200
     Total cpu time (sec):               0.66625


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1           -79.084241931608     0.3734E-01     0.7908E+02
    2           -79.085431671929     0.7567E-02     0.1190E-02
    3           -79.085759807021     0.9940E-03     0.3281E-03
    4           -79.085768585650     0.1833E-03     0.8779E-05
    5           -79.085769937826     0.3615E-04     0.1352E-05
    6           -79.085769725742     0.6004E-05     0.2121E-06
    7           -79.085769729164     0.2063E-05     0.3422E-08
    8           -79.085769729229     0.3646E-06     0.6527E-10
    9           -79.085769729230     0.3393E-07     0.9095E-12
   10           -79.085769729553     0.4633E-08     0.3225E-09
   11           -79.085769729585     0.9303E-09     0.3266E-10
   12           -79.085769729575     0.2416E-09     0.1073E-10
   13           -79.085769729575     0.4926E-10     0.4547E-12
   14           -79.085769729576     0.6369E-11     0.3837E-12
  ---------------------------------------------------------------
  Convergence criterion met in 14 iterations!

  - Ground state summary:

     Final ground state energy (a.u.):   -79.085769729576

     Correlation energy (a.u.):           -0.241918035945

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        1      5        0.015071367764
       14      4       -0.009516336778
        7      4        0.008547796583
       15      5       -0.006232215147
        5      6        0.005875107061
        6      2        0.005220429230
       13      5        0.005212699581
        2      4        0.005071084623
       11      6       -0.003616248565
        4      5        0.003233309227
     ------------------------------------

  - Finished solving the CC2 ground state equations

     Total wall time (sec):              0.12800
     Total cpu time (sec):               0.17152


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    ionize
     Excitation vectors:  right

  - Convergence thresholds

     Residual threshold:            0.1000E-10

     Number of singlet states:               2
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                10584
     Number of requested solutions:           2
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    2

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.541952125496    0.000000000000     0.4522E+00   0.5420E+00
     2   0.603654510207    0.000000000000     0.4058E+00   0.6037E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    4

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.372715900510    0.000000000000     0.2665E-01   0.1692E+00
     2   0.468895543449    0.000000000000     0.3909E-01   0.1348E+00
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373116259910    0.000000000000     0.1769E-02   0.4004E-03
     2   0.467137678521    0.000000000000     0.2111E-01   0.1758E-02
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:    8

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373117024943    0.000000000000     0.1933E-03   0.7650E-06
     2   0.467048184980    0.000000000000     0.3304E-02   0.8949E-04
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   10

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373115148544    0.000000000000     0.1556E-04   0.1876E-05
     2   0.467060022022    0.000000000000     0.4162E-04   0.1184E-04
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373115138206    0.000000000000     0.5136E-05   0.1034E-07
     2   0.467060159967    0.000000000000     0.3415E-05   0.1379E-06
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   14

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373115137826    0.000000000000     0.1229E-06   0.3800E-09
     2   0.467060160911    0.000000000000     0.6946E-07   0.9433E-09
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   16

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373115138353    0.000000000000     0.3232E-10   0.5277E-09
     2   0.467060161243    0.000000000000     0.1421E-10   0.3322E-09
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.373115138353    0.000000000000     0.6714E-14   0.6772E-14
     2   0.467060161243    0.000000000000     0.4073E-14   0.4531E-12
  ------------------------------------------------------------------------
  Convergence criterion met in 9 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.373115138353
     Fraction singles (|R1|/|R|):       0.964970111909

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       24      6       -0.964942350436
       24      3        0.005457394394
       24      5       -0.004851467453
       24      2       -0.000488378312
       24      4       -0.000137144561
       24      1        0.000001215215
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
     ------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.467060161243
     Fraction singles (|R1|/|R|):       0.969754565183

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
       24      5       -0.968158461578
       24      2        0.055330167335
       24      6        0.005190981608
       24      3        0.002148535422
       24      1       -0.000346487189
       24      4        0.000002043028
        6      1        0.000000000000
        7      1        0.000000000000
        8      1        0.000000000000
        9      1        0.000000000000
     ------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.373115138353       10.152980059638
        2                  0.467060161243       12.709354342153
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CC2 excited state equations (right)

     Total wall time (sec):              0.21000
     Total cpu time (sec):               0.22660

  - Timings for the CC2 excited state calculation

     Total wall time (sec):              0.78200
     Total cpu time (sec):               1.06760

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 11.024568 MB

  Total wall time in eT (sec):              1.80000
  Total cpu time in eT (sec):               2.79843

  Calculation ended: 2022-02-01 16:09:48 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
