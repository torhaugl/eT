


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:07:49 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: Xenon
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
        excited state
     end do

     method
        hf
        ccsd
     end method

     memory
        available: 8
     end memory

     frozen orbitals
        core
     end frozen orbitals

     solver scf
       algorithm:          scf-diis
       energy threshold:   1.0d-11
       gradient threshold: 1.0d-11
       print orbitals
     end solver scf

     solver cholesky
        threshold: 1.0d-12
     end solver cholesky

     solver cc gs
        energy threshold: 1.0d-10
        omega threshold:  1.0d-10
     end solver cc gs

     solver cc es
        singlet states: 3
        residual threshold: 1.0d-9
        right eigenvectors
     end solver cc es


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Xe     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1 Xe     0.000000000000     0.000000000000     0.000000000000        1
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               33
     Number of orthonormal atomic orbitals:   33

  - Molecular orbital details:

     Number of occupied orbitals:        27
     Number of virtual orbitals:          6
     Number of molecular orbitals:       33


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:             -7200.752736164746
     Number of electrons in guess:           54.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1         -7200.752736164754     0.5185E-11     0.7201E+04
  ---------------------------------------------------------------
  Convergence criterion met in 1 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  1.013566199136
     Nuclear repulsion energy:       0.000000000000
     Electronic energy:          -7200.752736164754
     Total energy:               -7200.752736164754

  - Timings for the RHF ground state calculation

     Total wall time (sec):              5.99300
     Total cpu time (sec):               9.74475

  - Preparation for frozen core approximation

     There are 18 frozen core orbitals.


  :: CCSD wavefunction
  =======================

     Bath orbital(s):         False

   - Number of orbitals:

     Occupied orbitals:    9
     Virtual orbitals:     6
     Molecular orbitals:   15
     Atomic orbitals:      33

   - Number of ground state amplitudes:

     Single excitation amplitudes:  54
     Double excitation amplitudes:  1485


  :: Excited state coupled cluster engine
  ==========================================

  Calculates the coupled cluster excitation vectors and excitation energies

  This is a CCSD excited state calculation.
  The following tasks will be performed:

     1) Cholesky decomposition of the electron repulsion integrals
     2) Preparation of MO basis and integrals
     3) Calculation of the ground state (diis algorithm)
     4) Calculation of the excited state (davidson algorithm)


  1) Cholesky decomposition of the electron repulsion integrals

   - Cholesky decomposition of electronic repulsion integrals solver
  ---------------------------------------------------------------------

  Performs a Cholesky decomposition of the two-electron electronic repulsion 
  integrals in the atomic orbital basis,

  (ab|cd) = sum_J L_ab^J L_cd^J.

  Once the Cholesky basis has been determined, the vectors L^J are constructed 
  and stored to disk. These may either be used directly, or be transformed 
  to the MO basis for use in post-HF calculations. For more information, 
  see S. D. Folkestad, E. F. Kjønstad and H. Koch, JCP, 150(19), (2019)

  - Cholesky decomposition settings:

     Target threshold is:   0.10E-11
     Span factor:           0.10E-01
     Max qual:                  1000

  - Cholesky decomposition ao details:

     Total number of AOs:                    33
     Total number of shell pairs:            91
     Total number of AO pairs:              561

     Significant shell pairs:                91
     Significant AO pairs:                  561

     Construct shell pairs:                  91
     Construct AO pairs:                    561

  Iter.  #Sign. ao pairs / shell pairs   Max diagonal    #Qualified    #Cholesky    Cholesky array size
  -------------------------------------------------------------------------------------------------------
     1               548 /      87       0.34285E+02          70             25             13700
     2               463 /      77       0.18943E+00         316            107             49541
     3               393 /      65       0.12693E-02         280            169             66417
     4               236 /      50       0.11225E-04         213            233             54988
     5               177 /      36       0.80317E-07         105            280             49560
     6                36 /       6       0.55120E-09          81            305             10980
     7                 0 /       0       0.35523E-11          28            319                 0
  -------------------------------------------------------------------------------------------------------

  - Summary of Cholesky decomposition of electronic repulsion integrals:

     Final number of Cholesky vectors: 319

 - Testing the Cholesky decomposition decomposition electronic repulsion integrals:

     Maximal difference between approximate and actual diagonal:              0.7788E-12
     Minimal element of difference between approximate and actual diagonal:  -0.6578E-14

  - Settings for integral handling:

     Cholesky vectors in memory: True
     ERI matrix in memory:       True

  - Finished decomposing the ERIs.

     Total wall time (sec):              1.70600
     Total cpu time (sec):               2.28243


  2) Preparation of MO basis and integrals


  3) Calculation of the ground state (diis algorithm)

   - DIIS coupled cluster ground state solver
  ----------------------------------------------

  A DIIS CC ground state amplitude equations solver. It uses an extrapolation 
  of previous quasi-Newton perturbation theory estimates of the next amplitudes. 
  See Helgaker et al., Molecular Electronic Structure Theory, Chapter 
  13.

  - CC ground state solver settings:

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09
     Max number of iterations:      100

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (cc_gs_diis_errors): file
     Storage (cc_gs_diis_parameters): file

  Iteration    Energy (a.u.)        |omega|       Delta E (a.u.)
  ---------------------------------------------------------------
    1         -7200.772576367114     0.2520E-01     0.7201E+04
    2         -7200.775707261630     0.6690E-02     0.3131E-02
    3         -7200.776584130716     0.6545E-03     0.8769E-03
    4         -7200.776576927907     0.8817E-04     0.7203E-05
    5         -7200.776578333653     0.8217E-05     0.1406E-05
    6         -7200.776578229645     0.5076E-06     0.1040E-06
    7         -7200.776578227869     0.6604E-07     0.1775E-08
    8         -7200.776578228645     0.6776E-08     0.7758E-09
    9         -7200.776578228858     0.5988E-09     0.2128E-09
   10         -7200.776578228852     0.3879E-10     0.5457E-11
  ---------------------------------------------------------------
  Convergence criterion met in 10 iterations!

  - Ground state summary:

     Final ground state energy (a.u.): -7200.776578228852

     Correlation energy (a.u.):           -0.023842064099

     Largest single amplitudes:
     -----------------------------------
        a       i         t(a,i)
     -----------------------------------
        3      7       -0.003311414566
        4      8       -0.003295592691
        2      9       -0.003295583533
        1      6       -0.002078763040
        5      6        0.000865520218
        2      8       -0.000323442492
        4      9        0.000323431961
        3      9       -0.000008933388
        2      7        0.000008543601
        4      7       -0.000004408965
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         t(ai,bj)
     --------------------------------------------------
        3      7       3      7       -0.030115518544
        4      8       4      8       -0.029859994675
        2      9       2      9       -0.029859847117
        3      7       4      8       -0.020484688335
        3      7       2      9       -0.020484596996
        4      8       2      9       -0.020327418734
        1      6       3      7       -0.019777288776
        1      6       4      8       -0.019682793274
        1      6       2      9       -0.019682738574
        1      6       1      6       -0.016915203986
     --------------------------------------------------

  - Finished solving the CCSD ground state equations

     Total wall time (sec):              0.14100
     Total cpu time (sec):               0.13148


  4) Calculation of the excited state (davidson algorithm)
     Calculating right vectors

   - Davidson coupled cluster excited state solver
  ---------------------------------------------------

  A Davidson solver that calculates the lowest eigenvalues and the right 
  or left eigenvectors of the Jacobian matrix, A. The eigenvalue problem 
  is solved in a reduced space, the dimension of which is expanded until 
  the convergence criteria are met.

  A complete description of the algorithm can be found in E. R. Davidson, 
  J. Comput. Phys. 17, 87 (1975).

  - Settings for coupled cluster excited state solver (Davidson):

     Calculation type:    valence
     Excitation vectors:  right

  - Convergence thresholds

     Residual threshold:            0.1000E-08

     Number of singlet states:               3
     Max number of iterations:             100

   - Davidson tool settings:

     Number of parameters:                 1539
     Number of requested solutions:           3
     Max reduced space dimension:           100

     Storage (cc_es_davidson_trials): file
     Storage (cc_es_davidson_transforms): file

  Iteration:                  1
  Reduced space dimension:    3

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.723119366897    0.000000000000     0.1258E+00   0.7231E+00
     2   0.723119366897    0.000000000000     0.1258E+00   0.7231E+00
     3   0.723119366897    0.000000000000     0.1258E+00   0.7231E+00
  ------------------------------------------------------------------------

  Iteration:                  2
  Reduced space dimension:    6

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714272477406    0.000000000000     0.2568E-01   0.8847E-02
     2   0.714272477406    0.000000000000     0.2568E-01   0.8847E-02
     3   0.714272477406    0.000000000000     0.2568E-01   0.8847E-02
  ------------------------------------------------------------------------

  Iteration:                  3
  Reduced space dimension:    9

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714035385960    0.000000000000     0.4892E-02   0.2371E-03
     2   0.714035385960    0.000000000000     0.4892E-02   0.2371E-03
     3   0.714035385960    0.000000000000     0.4892E-02   0.2371E-03
  ------------------------------------------------------------------------

  Iteration:                  4
  Reduced space dimension:   12

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714034035977    0.000000000000     0.1060E-02   0.1350E-05
     2   0.714034035977    0.000000000000     0.1060E-02   0.1350E-05
     3   0.714034035977    0.000000000000     0.1060E-02   0.1350E-05
  ------------------------------------------------------------------------

  Iteration:                  5
  Reduced space dimension:   15

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028360263    0.000000000000     0.1695E-03   0.5676E-05
     2   0.714028360263    0.000000000000     0.1695E-03   0.5676E-05
     3   0.714028360263    0.000000000000     0.1695E-03   0.5676E-05
  ------------------------------------------------------------------------

  Iteration:                  6
  Reduced space dimension:   18

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028516803    0.000000000000     0.2831E-04   0.1565E-06
     2   0.714028516803    0.000000000000     0.2831E-04   0.1565E-06
     3   0.714028516803    0.000000000000     0.2831E-04   0.1565E-06
  ------------------------------------------------------------------------

  Iteration:                  7
  Reduced space dimension:   21

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028569511    0.000000000000     0.3393E-05   0.5271E-07
     2   0.714028569511    0.000000000000     0.3393E-05   0.5271E-07
     3   0.714028569511    0.000000000000     0.3393E-05   0.5271E-07
  ------------------------------------------------------------------------

  Iteration:                  8
  Reduced space dimension:   24

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028567558    0.000000000000     0.2232E-06   0.1953E-08
     2   0.714028567558    0.000000000000     0.2232E-06   0.1953E-08
     3   0.714028567558    0.000000000000     0.2232E-06   0.1953E-08
  ------------------------------------------------------------------------

  Iteration:                  9
  Reduced space dimension:   27

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028567631    0.000000000000     0.4254E-07   0.7304E-10
     2   0.714028567631    0.000000000000     0.4254E-07   0.7304E-10
     3   0.714028567631    0.000000000000     0.4254E-07   0.7303E-10
  ------------------------------------------------------------------------

  Iteration:                 10
  Reduced space dimension:   30

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028567738    0.000000000000     0.5067E-08   0.1075E-09
     2   0.714028567738    0.000000000000     0.5067E-08   0.1075E-09
     3   0.714028567738    0.000000000000     0.5067E-08   0.1075E-09
  ------------------------------------------------------------------------

  Iteration:                 11
  Reduced space dimension:   33

   Root  Eigenvalue (Re)   Eigenvalue (Im)    |residual|   Delta E (Re)
  ------------------------------------------------------------------------
     1   0.714028567742    0.000000000000     0.6138E-09   0.3411E-11
     2   0.714028567742    0.000000000000     0.6138E-09   0.3407E-11
     3   0.714028567742    0.000000000000     0.6138E-09   0.3406E-11
  ------------------------------------------------------------------------
  Convergence criterion met in 11 iterations!

  - Excitation vector amplitudes:

     Electronic state nr. 1

     Energy (Hartree):                  0.714028567742
     Fraction singles (|R1|/|R|):       0.997705247338

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      9       -0.963822596282
        1      8       -0.250761309745
        2      6       -0.058148691884
        4      6       -0.009187318791
        5      9       -0.009054320985
        3      1        0.002769522160
        4      4        0.002743153224
        5      8       -0.002355696367
        2      2        0.002310419649
        2      5       -0.001648241454
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        1      9       2      9       -0.040374083870
        1      7       3      9       -0.021135338906
        1      8       4      9       -0.020622806541
        1      6       2      6       -0.018701333385
        1      6       1      9       -0.018069983237
        3      7       1      9       -0.016197180324
        4      8       1      9       -0.015582771458
        4      6       4      9        0.010802006226
        3      6       3      9        0.010711886981
        1      8       4      8       -0.010175301018
     --------------------------------------------------

     Electronic state nr. 2

     Energy (Hartree):                  0.714028567742
     Fraction singles (|R1|/|R|):       0.997705247338

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      8        0.959105873968
        1      9       -0.267149048749
        4      6        0.057963864222
        1      7       -0.024161880446
        2      6       -0.010174806812
        5      8        0.009010011256
        2      4       -0.002768350923
        5      9       -0.002509645704
        4      2        0.002284091998
        3      3        0.002244390041
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        1      8       4      8        0.040182128213
        1      7       3      8        0.021032393793
        2      8       1      9        0.020492897859
        1      6       4      6        0.018641890539
        1      6       1      8        0.017981552966
        3      7       1      8        0.016118549662
        1      8       2      9        0.015468721890
        1      9       2      9       -0.010861682202
        2      6       2      8       -0.010759272189
        3      6       3      8       -0.010659319762
     --------------------------------------------------

     Electronic state nr. 3

     Energy (Hartree):                  0.714028567742
     Fraction singles (|R1|/|R|):       0.997705247338

     Largest single amplitudes:
     -----------------------------------
        a       i         R(a,i)
     -----------------------------------
        1      7        0.995585407227
        3      6        0.058851973966
        1      8       -0.025370578208
        5      7        0.009352706483
        3      5       -0.003213661273
        2      1       -0.002772318602
        4      3        0.002207796862
        4      2       -0.001654939787
        1      9       -0.001587095706
        4      6       -0.001405009732
     ------------------------------------

     Largest double amplitudes:
     --------------------------------------------------
        a      i       b      j         R(ai,bj)
     --------------------------------------------------
        1      7       3      7        0.041821803751
        2      7       1      9        0.021727603315
        4      7       1      8        0.021727027212
        1      6       3      6        0.018927517539
        1      6       1      7        0.018665480233
        1      7       2      9        0.016651130523
        1      7       4      8        0.016650346263
        4      6       4      7       -0.011065105477
        2      6       2      7       -0.011064816176
        1      6       5      7       -0.008201808251
     --------------------------------------------------

     - Electronic excitation energies:

                                      Excitation energy
                          ------------------------------------------
      State                (Hartree)             (eV)
     ---------------------------------------------------------------
        1                  0.714028567742       19.429706986122
        2                  0.714028567742       19.429706986122
        3                  0.714028567742       19.429706986123
     ---------------------------------------------------------------
     eV/Hartree (CODATA 2014): 27.21138602

  - Finished solving the CCSD excited state equations (right)

     Total wall time (sec):              0.51000
     Total cpu time (sec):               0.41590

  - Timings for the CCSD excited state calculation

     Total wall time (sec):              2.36200
     Total cpu time (sec):               2.83481

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 3.270288 MB

  Total wall time in eT (sec):              8.53700
  Total cpu time in eT (sec):              12.84250

  Calculation ended: 2022-02-01 16:07:58 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713
     Cholesky decomposition of ERIs: https://doi.org/10.1063/1.5083802

  eT terminated successfully!
