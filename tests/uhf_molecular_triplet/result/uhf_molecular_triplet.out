


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:08:49 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: O2
        charge: 0
        multiplicity: 3
     end system

     method
        uhf
     end method

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-12
        gradient threshold: 1.0d-12
     end solver scf

     do
        ground state
     end do


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: UHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  O     0.000000000000     0.000000000000     1.208000000000        2
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: cc-pvdz
        1  O     0.000000000000     0.000000000000     0.000000000000        1
        2  O     0.000000000000     0.000000000000     2.282789158475        2
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               28
     Number of orthonormal atomic orbitals:   28

  - Molecular orbital details:

     Number of alpha electrons:               9
     Number of beta electrons:                7
     Number of virtual alpha orbitals:       19
     Number of virtual beta orbitals:        21
     Number of molecular orbitals:           28


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a UHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -149.807063350854
     Number of electrons in guess:           16.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-17
     Exchange screening threshold:   0.1000E-15
     ERI cutoff:                     0.1000E-17
     One-electron integral  cutoff:  0.1000E-22
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-11
     Energy threshold:              0.1000E-11

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -149.596896918359     0.3404E-01     0.1496E+03
     2          -149.624445236441     0.1072E-01     0.2755E-01
     3          -149.627146199876     0.4432E-02     0.2701E-02
     4          -149.627617074631     0.9473E-03     0.4709E-03
     5          -149.627667089032     0.1705E-03     0.5001E-04
     6          -149.627668974224     0.2233E-04     0.1885E-05
     7          -149.627668990367     0.2951E-05     0.1614E-07
     8          -149.627668990686     0.1299E-06     0.3193E-09
     9          -149.627668990687     0.1916E-07     0.6821E-12
    10          -149.627668990687     0.4015E-08     0.2842E-13
    11          -149.627668990687     0.6182E-09     0.5684E-13
    12          -149.627668990687     0.1169E-09     0.8527E-13
    13          -149.627668990687     0.2381E-10     0.0000E+00
    14          -149.627668990687     0.2427E-11     0.1421E-12
    15          -149.627668990687     0.5278E-12     0.5684E-13
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of UHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.976726296347
     HOMO-LUMO gap (beta):           0.687048046170
     Nuclear repulsion energy:      28.035878724238
     Electronic energy:           -177.663547714925
     Total energy:                -149.627668990687

  - UHF wavefunction spin expectation values:

     Sz:                   1.00000000
     Sz(Sz + 1):           2.00000000
     S^2:                  2.03307861
     Spin contamination:   0.03307861

  - Timings for the UHF ground state calculation

     Total wall time (sec):              4.30400
     Total cpu time (sec):               6.66904

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 280.888 KB

  Total wall time in eT (sec):              4.35800
  Total cpu time in eT (sec):               6.72166

  Calculation ended: 2022-02-01 16:08:53 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
