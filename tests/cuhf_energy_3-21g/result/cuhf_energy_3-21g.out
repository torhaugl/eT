


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:10:26 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: HOF He
        charge: 0
        multiplicity: 1
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-10
        gradient threshold: 1.0d-10
     end solver scf

     method
        cuhf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: CUHF wavefunction
  =======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  F    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 3-21g
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  F    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               22
     Number of orthonormal atomic orbitals:   22

  - Molecular orbital details:

     Number of alpha electrons:              10
     Number of beta electrons:               10
     Number of virtual alpha orbitals:       12
     Number of virtual beta orbitals:        12
     Number of molecular orbitals:           22


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a CUHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:              -177.433645515540
     Number of electrons in guess:           20.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-15
     Exchange screening threshold:   0.1000E-13
     ERI cutoff:                     0.1000E-15
     One-electron integral  cutoff:  0.1000E-20
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-09
     Energy threshold:              0.1000E-09

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1          -176.427944702642     0.2431E-01     0.1764E+03
     2          -176.447985480619     0.5087E-02     0.2004E-01
     3          -176.449095710219     0.2785E-02     0.1110E-02
     4          -176.449182641423     0.6916E-03     0.8693E-04
     5          -176.449192645236     0.1227E-03     0.1000E-04
     6          -176.449193031189     0.4894E-04     0.3860E-06
     7          -176.449193080484     0.4074E-05     0.4929E-07
     8          -176.449193081522     0.5832E-06     0.1038E-08
     9          -176.449193081558     0.1107E-06     0.3632E-10
    10          -176.449193081559     0.4086E-07     0.1364E-11
    11          -176.449193081559     0.1308E-07     0.0000E+00
    12          -176.449193081560     0.3627E-08     0.1421E-12
    13          -176.449193081560     0.2626E-09     0.8527E-13
    14          -176.449193081560     0.1181E-09     0.2842E-13
    15          -176.449193081560     0.3209E-10     0.0000E+00
  ---------------------------------------------------------------
  Convergence criterion met in 15 iterations!

  - Summary of CUHF wavefunction energetics (a.u.):

     HOMO-LUMO gap (alpha):          0.650262283399
     HOMO-LUMO gap (beta):           0.650262283399
     Nuclear repulsion energy:      48.518317619727
     Electronic energy:           -224.967510701287
     Total energy:                -176.449193081560

  - CUHF wavefunction spin expectation values:

     Sz:                   0.00000000
     Sz(Sz + 1):           0.00000000
     S^2:                  0.00000000
     Spin contamination:   0.00000000

  - Timings for the CUHF ground state calculation

     Total wall time (sec):              0.24500
     Total cpu time (sec):               0.33610

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 179.316 KB

  Total wall time in eT (sec):              0.26200
  Total cpu time in eT (sec):               0.35320

  Calculation ended: 2022-02-01 16:10:26 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
