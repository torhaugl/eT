


                     eT 1.6 - an electronic structure program

  ------------------------------------------------------------------------
   Author list in alphabetical order:
  ------------------------------------------------------------------------
   J. H. Andersen, A. Balbi, S. Coriani, S. D. Folkestad, T. Giovannini, 
   L. Goletto, T. S. Haugland, A. Hutcheson, I-M. Høyvik, E. F. Kjønstad, 
   H. Koch, R. Matveeva, T. Moitra, R. H. Myhre, A. C. Paul, S. Roet, 
   M. Scavino, A. K. Schnack-Petersen, A. S. Skeidsvoll, Å. H. Tveten
  ------------------------------------------------------------------------
   J. Chem. Phys. 152, 184103 (2020); https://doi.org/10.1063/5.0004713


   This is eT 1.6.0 Galaxy
  ------------------------------------------------------------
  Configuration date: 2022-02-01 15:59:21 UTC +01:00
  Git branch:         release-v1.6.0
  Git hash:           2abe95f42f8b4eabde880c038dd204ec81d587da
  Fortran compiler:   GNU 10.3.0
  C compiler:         GNU 10.3.0
  C++ compiler:       GNU 10.3.0
  LAPACK type:        SYSTEM_NATIVE
  BLAS type:          SYSTEM_NATIVE
  64-bit integers:    OFF
  OpenMP:             ON
  PCM:                OFF
  Forced batching:    OFF
  Runtime checks:     OFF
  ------------------------------------------------------------

  Calculation started: 2022-02-01 16:09:03 UTC +01:00


  :: Input file
  ================

     Note: geometry section is excluded from this print

     system
        name: H2O He
        charge: 0
     end system

     do
        ground state
     end do

     memory
        available: 8
     end memory

     solver scf
        algorithm:          scf-diis
        energy threshold:   1.0d-11
        gradient threshold: 1.0d-11
        write molden
        print orbitals
     end solver scf

     method
        hf
     end method


  Running on 2 OMP threads
  Memory available for calculation: 8.000000 GB


  :: RHF wavefunction
  ======================

     ==============================================================================
                                     Geometry (angstrom)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g(2df,p)
        1  H     0.866810000000     0.601440000000     5.000000000000        1
        2  H    -0.866810000000     0.601440000000     5.000000000000        2
        3  O     0.000000000000    -0.075790000000     5.000000000000        3
        4 He     0.000000000000     0.000000000000     7.500000000000        4
     ==============================================================================

     ==============================================================================
                                     Geometry (a.u.)
     ==============================================================================
        Atom           X                  Y                  Z         # in input
     ==============================================================================
        Basis: 6-31g(2df,p)
        1  H     1.638033502034     1.136556880358     9.448630622825        1
        2  H    -1.638033502034     1.136556880358     9.448630622825        2
        3  O     0.000000000000    -0.143222342981     9.448630622825        3
        4 He     0.000000000000     0.000000000000    14.172945934238        4
     ==============================================================================

  - Cholesky decomposition of AO overlap to get linearly independent AOs:

     Linear dependence threshold:             0.10E-05
     Number of atomic orbitals:               55
     Number of orthonormal atomic orbitals:   55

  - Molecular orbital details:

     Number of occupied orbitals:         6
     Number of virtual orbitals:         49
     Number of molecular orbitals:       55


  :: Hartree-Fock engine
  =========================

  Drives the calculation of the Hartree-Fock state.

  This is a RHF ground state calculation.
  The following tasks will be performed:

     1) Generate initial SAD density
     2) Calculation of reference state (SCF-DIIS algorithm)


  1) Generate initial SAD density


  2) Calculation of reference state (SCF-DIIS algorithm)

  - Setting initial AO density to sad

     Energy of initial guess:               -78.484417586941
     Number of electrons in guess:           12.000000000000

  - Screening and integral thresholds:

     Coulomb screening threshold:    0.1000E-16
     Exchange screening threshold:   0.1000E-14
     ERI cutoff:                     0.1000E-16
     One-electron integral  cutoff:  0.1000E-21
     Cumulative Fock threshold:      0.1000E+01

  - SCF solver settings:

     Maximum iterations:                   100
     Acceleration type:                   diis

  - Convergence thresholds

     Residual threshold:            0.1000E-10
     Energy threshold:              0.1000E-10

  - DIIS tool settings:

     DIIS dimension:   8

     Storage (solver scf_errors): memory
     Storage (solver scf_parameters): memory

  Iteration       Energy (a.u.)      Max(grad.)    Delta E (a.u.)
  ---------------------------------------------------------------
     1           -78.797967663339     0.9846E-01     0.7880E+02
     2           -78.828051235097     0.4580E-01     0.3008E-01
     3           -78.844390947122     0.5686E-02     0.1634E-01
     4           -78.844671885505     0.1871E-02     0.2809E-03
     5           -78.844712382238     0.3969E-03     0.4050E-04
     6           -78.844714473163     0.5479E-04     0.2091E-05
     7           -78.844714511466     0.3013E-05     0.3830E-07
     8           -78.844714511775     0.8681E-06     0.3086E-09
     9           -78.844714511793     0.2748E-06     0.1828E-10
    10           -78.844714511796     0.4003E-07     0.2373E-11
    11           -78.844714511796     0.1118E-07     0.5684E-13
    12           -78.844714511796     0.2816E-08     0.1421E-13
    13           -78.844714511796     0.1160E-08     0.9948E-13
    14           -78.844714511796     0.3002E-09     0.1421E-13
    15           -78.844714511796     0.2935E-10     0.5684E-13
    16           -78.844714511796     0.3526E-11     0.4263E-13
  ---------------------------------------------------------------
  Convergence criterion met in 16 iterations!

  - Summary of RHF wavefunction energetics (a.u.):

     HOMO-LUMO gap:                  0.666604330128
     Nuclear repulsion energy:      12.167611407170
     Electronic energy:            -91.012325918966
     Total energy:                 -78.844714511796

  - Timings for the RHF ground state calculation

     Total wall time (sec):              2.80400
     Total cpu time (sec):               4.86048

  ------------------------------------------------------------

  Peak memory usage during the execution of eT: 637.680 KB

  Total wall time in eT (sec):              2.82800
  Total cpu time in eT (sec):               4.88460

  Calculation ended: 2022-02-01 16:09:06 UTC +01:00

  - Implementation references:

     eT: https://doi.org/10.1063/5.0004713

  eT terminated successfully!
